'''
En este archivo se almacenan las funciones de preprocesado, comparación de métodos de cálculo del voltaje del plasma, y funciones de
exportación a csv y Excel.
'''

import pandas as pd
from scipy.signal import savgol_filter
import os
import csv
from openpyxl import Workbook

def PreProcesado(nombre_archivo, dif_corriente, freq, v_min, v_max):
    # Extraer la información del archivo a un df
    df = pd.read_excel(nombre_archivo)

    # Recortar las columnas que no son de utilidad
    if "Time" in df.columns:
        del df["Time"]
    if "Time 1" in df.columns:
        del df["Time 1"]

    # Cambiar los nombres
    df.rename(columns={df.columns[0]: "Voltaje", df.columns[1]: "Corriente"}, inplace=True)

    # Calcular la diferencia entre las filas de Corriente
    df['Diferencia Corriente'] = df["Corriente"].diff()

    # Lista para almacenar las diferencias mayores a dif_corriente
    diferencias_mayores = []

    # Iterar sobre el df y encontrar las diferencias mayores a dif_corriente
    for index, row in df.iterrows():
        if abs(row['Diferencia Corriente']) > dif_corriente:
            diferencias_mayores.append(index)

    df = df.reindex(columns=['Tiempo', 'Voltaje', 'Corriente', 'Diferencia Corriente'])

    ##########################################################
    # Todo esto es antes de segmentar el df, por lo que los  #
    # cambios de forma al df se realizan antes de esta parte #
    ##########################################################

    # Lista para almacenar los DataFrames segmentados
    segmentos_df = []
    # Variable para guardar el inicio de cada segmento
    inicio_segmento = 0

    for indice in diferencias_mayores:
        # Extraer el segmento desde inicio_segmento hasta el índice actual
        segmento = df.iloc[inicio_segmento:indice].copy()

        # Renombrar las columnas
        segmento.rename(columns={segmento.columns[1]: 'Voltaje', segmento.columns[2]: 'Corriente'}, inplace=True)

        # Agregar el segmento a la lista de segmentos_df
        segmentos_df.append(segmento)

        # Actualizar el inicio del próximo segmento
        inicio_segmento = indice + 1

    # Eliminar el primer segmento (ceros)
    segmentos_df.pop(0)

    for segmento in segmentos_df:
        corriente_suavizada = savgol_filter(segmento['Corriente'], window_length=11, polyorder=3, mode='nearest')
        segmento['Corriente'] = corriente_suavizada


    # Verificar si el primer segmento cumple con que tenga una longitud aceptable, esté dentro del voltaje especificado,
    # tenga mínimo 2 datos negativos y haya valores por debajo de v_min
    if (
        len(segmentos_df[0]) <= 50
        or not segmentos_df[0]['Voltaje'].between(v_min, v_max).all()
        or segmentos_df[0]['Voltaje'].lt(0).sum() <= 2
        or segmentos_df[0]['Voltaje'].gt(0).sum() >= 2
        or segmentos_df[0]['Voltaje'].lt(v_min).any()
    ):
        segmentos_df = segmentos_df[1:]


    tiempo_acumulado = 0
    segmentos_validos = []

    for segmento in segmentos_df:
        if len(segmento) > 0:
            segmento['Tiempo'] = tiempo_acumulado + (segmento.index - segmento.index[0]) / freq
            tiempo_acumulado = segmento['Tiempo'].iloc[-1]
            segmentos_validos.append(segmento)
        else:
            segmento['Tiempo'] = []

    segmentos_df = segmentos_validos

    # Itera sobre cada DataFrame en la lista segmentos_df y elimina la última fila
    for i in range(len(segmentos_df)):
        segmentos_df[i] = segmentos_df[i].iloc[:-1]

    # Si deseas eliminar completamente los DataFrames que quedaron vacíos después de quitar la última fila, puedes hacer lo siguiente
    segmentos_df = [df for df in segmentos_df if not df.empty]



    return df, segmentos_df

def PreProcesado2(nombre_archivo, freq, v_min, v_max):
    # Extraer la información del archivo a un df
    df = pd.read_excel(nombre_archivo)
    # Recortar las columnas que no son de utilidad
    if "Time" in df.columns:
        del df["Time"]
    if "Time 1" in df.columns:
        del df["Time 1"]


    # Cambiar los nombres
    df.rename(columns={df.columns[0]: "Voltaje", df.columns[1]: "Corriente"}, inplace=True)

    #Elimina el primer valor de cada df. Esto porque a veces el primer valor no se registra como número y el código muere. Este valor no importa pues al principio siempre se mide ruido.
    df["Voltaje"].pop(0)
    df["Corriente"].pop(0)


    # Calcular la diferencia entre las filas VOLTAJE y CORRIENTE
    df['Diferencia VOLT'] = df["Voltaje"].diff()

    df['Diferencia CORR'] = df["Corriente"].diff()

    # Lista para almacenar las diferencias mayores al criterio establecido.
    diferencias_mayores = []

    # Iterar sobre el df y encontrar las diferencias mayores a un cuarto del rango de la señal diente de sierra. Esto detecta cada pico del diente de sierra individual.
    for index, row in df.iterrows():
        if abs(row['Diferencia VOLT']) > (v_max-v_min)*0.25:
            diferencias_mayores.append(index)
    # print(len(diferencias_mayores))
    df = df.reindex(columns=['Tiempo', 'Voltaje', 'Corriente', 'Diferencia VOLT', 'Diferencia CORR'])

    ##########################################################
    # Todo esto es antes de segmentar el df, por lo que los  #
    # cambios de forma al df se realizan antes de esta parte #
    ##########################################################

    # Lista para almacenar los DataFrames segmentados
    segmentos_df = []
    # Variable para guardar el inicio de cada segmento
    inicio_segmento = 0

    for indice in diferencias_mayores:
        # Extraer el segmento desde inicio_segmento hasta el índice actual
        segmento = df.iloc[inicio_segmento:indice].copy()

        # Renombrar las columnas
        segmento.rename(columns={segmento.columns[1]: 'Voltaje', segmento.columns[2]: 'Corriente'}, inplace=True)

        # Agregar el segmento a la lista de segmentos_df
        segmentos_df.append(segmento)

        # Actualizar el inicio del próximo segmento
        inicio_segmento = indice + 1

    # Eliminar el primer segmento (ceros)
    segmentos_df.pop(0)

    for segmento in segmentos_df:
        corriente_suavizada = savgol_filter(segmento['Corriente'], window_length=11, polyorder=3, mode='nearest')
        segmento['Corriente'] = corriente_suavizada


    # Verificar si el primer segmento cumple con que tenga una longitud aceptable, esté dentro del voltaje especificado,
    # tenga mínimo 2 datos negativos y haya valores por debajo de v_min
    if (
        len(segmentos_df[0]) <= 50
        or not segmentos_df[0]['Voltaje'].between(v_min, v_max).all()
        or segmentos_df[0]['Voltaje'].lt(0).sum() <= 2
        or segmentos_df[0]['Voltaje'].gt(0).sum() >= 2
        or segmentos_df[0]['Voltaje'].lt(v_min).any()
    ):
        segmentos_df = segmentos_df[1:]

    # print(len(segmentos_df))


    
    SegValid = []
    Rangos = []

    #Cálculo del rango de cada segmento.
    for i in range(len(segmentos_df)-1):
        if len(segmentos_df[i])>0:
            a = max(segmentos_df[i]['Corriente']) - min(segmentos_df[i]['Corriente'])
            Rangos.append(a)
    #Filtro de segmentos. Todos los segmentos cuyo rango sea menor al 10% del segmento más grande, no son considerados válidos. (Filtro más potente).
    for i in range(len(segmentos_df)-1):
        if len(segmentos_df[i])>0:
            if max(segmentos_df[i]['Corriente']) - min(segmentos_df[i]['Corriente']) >= max(Rangos)*0.1:
                SegValid.append(segmentos_df[i]) 
    
    segmentos_df = SegValid

    #Elimina el primer y el último segmento válido, pues suelen estar incompletos debido al corte abrupto de la descarga
    segmentos_df.pop(-1)
    segmentos_df.pop(0)

    tiempo_acumulado = 0
    segmentos_validos = []

    for segmento in segmentos_df:
        if len(segmento) > 0:
            segmento['Tiempo'] = tiempo_acumulado + (segmento.index - segmento.index[0]) / freq
            tiempo_acumulado = segmento['Tiempo'].iloc[-1]
            segmentos_validos.append(segmento)
        else:
            segmento['Tiempo'] = []
    segmentos_df = segmentos_validos

    # Itera sobre cada DataFrame en la lista segmentos_df y elimina la última fila
    for i in range(len(segmentos_df)):
        segmentos_df[i] = segmentos_df[i].iloc[:-1]

    # Si deseas eliminar completamente los DataFrames que quedaron vacíos después de quitar la última fila, puedes hacer lo siguiente
    segmentos_df = [df for df in segmentos_df if not df.empty]

    # print(len(segmentos_df))
    return df, segmentos_df

def Comparacion_Vp(lista_vp_sd,lista_vp_rectas,lista_vp_teorico):

    print("Vp, Método Segunda Derivada: No depende de ninguna función")
    for vp in lista_vp_sd:
        print(f"{vp:.5f}")

    print("\nVp, Método Intersección de Rectas: Depende de Vf y es visual")
    for vp in lista_vp_rectas:
        print(f"{vp:.5f}")

    print("\nVp, Método Teórico: Depende de TeV y Vf")
    for vp in lista_vp_teorico:
        print(f"{vp:.5f}")

def Exportar_Resultados_CSV(nombre_archivo,listas):
    # Verificar si el archivo ya existe
    if os.path.exists(nombre_archivo):
        raise FileExistsError(f"El archivo '{nombre_archivo}' ya existe. Por favor, elija un nombre de archivo diferente o elimine el archivo existente.")

    # Abre el archivo CSV en modo escritura
    with open(nombre_archivo, 'w', newline='') as archivo_csv:
        # Crea un escritor CSV
        escritor = csv.writer(archivo_csv, delimiter=',')

        # Escribe el nombre del archivo en la primera fila
        escritor.writerow([nombre_archivo])

        # Escribe las listas en filas separadas
        for lista in listas:
            escritor.writerow([lista[0], *lista[1:]])

def Graf_Iis_Vf_to_XLSX(segmentos_df, lista_Iis, lista_Vf, output_xlsx):
    vf_list = []  # Lista para almacenar los valores de vf
    t_list = []   # Lista para almacenar los valores de t

    for i, segmento in enumerate(segmentos_df):
        t = segmento['Tiempo'].iloc[-1]
        vf = lista_Vf[i]
        vf_list.append(vf)
        t_list.append(t)

    # Crear un DataFrame con los datos de vf y t
    data = {'Tiempo [s]': t_list, 'Vf [V]': vf_list}
    df = pd.DataFrame(data)

    # Crear un libro de trabajo y agregar una hoja de cálculo
    wb = Workbook()
    ws = wb.active

    # Agregar los datos del DataFrame a la hoja de cálculo
    for r_idx, row in enumerate(df.to_dict(orient='records'), start=2):
        for c_idx, value in enumerate(row.values(), start=1):
            ws.cell(row=r_idx, column=c_idx, value=value)

    # Añadir encabezados
    for c_idx, header in enumerate(df.columns, start=1):
        ws.cell(row=1, column=c_idx, value=header)

    # Guardar el libro de trabajo en un archivo xlsx
    wb.save(output_xlsx)

def Exportar_Excel(segmentos_df,nombre_archivo):

    # Verificar si el nombre del archivo tiene la extensión .xlsx
    if not nombre_archivo.endswith(".xlsx"):
        nombre_archivo += ".xlsx"

    # Crear un objeto ExcelWriter para escribir en el archivo
    writer = pd.ExcelWriter(nombre_archivo, engine='xlsxwriter')

    # Iterar sobre cada DataFrame en segmentos_df y escribirlo en una hoja separada
    for i, segmento in enumerate(segmentos_df):
        # Obtener el nombre de la hoja a partir del número de segmento
        nombre_hoja = f'Segmento_{i+1}'

        # Escribir el DataFrame en la hoja correspondiente
        segmento.to_excel(writer, sheet_name=nombre_hoja, index=False)

    # Cerrar el objeto ExcelWriter para guardar los cambios
    writer.close()

def list_to_csv(filename, data):
    with open(filename, 'w', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        writer.writerows(data)

    print(f'Archivo {filename} creado exitosamente.')
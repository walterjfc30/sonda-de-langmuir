'''
En este archivo se almacenan las funciones involucradas en graficar los datos y parámetros calculados.
'''

import matplotlib.pyplot as plt
import numpy as np
import csv
import pandas as pd

def GrafDatos(df,segmentos_df,eje_x1,eje_y1,eje_x2,eje_y2,freq,num_archivo,show_legend=True):

    # Graficar una variable (como la corriente) de todo el dataframe respecto a una variable
    x = np.linspace(0, (len(df[eje_y1]) - 1) / freq, len(df[eje_y1]))
    plt.figure(figsize=(10, 6))
    plt.plot(x, df[eje_y1])
    plt.xlabel(eje_x1)
    plt.ylabel(eje_y1)
    plt.title('Gráficas de '+eje_y1+' vs. '+eje_x1+', Archivo: '+str(num_archivo))
    plt.grid(True)
    plt.show()
    
    # Graficar una variable de los segmentos respecto a otra variable
    plt.figure(figsize=(10, 6))
    for i, segmento in enumerate(segmentos_df):
        plt.plot(segmento[eje_x1], segmento[eje_y1], label=f"Segmento {i+1}")
    plt.xlabel(eje_x1)
    plt.ylabel(eje_y1)
    plt.title('Gráficas de '+eje_y1+' vs. '+eje_x1+', Archivo: '+str(num_archivo))
    if show_legend==True:
        plt.legend()
    plt.grid(True)
    plt.show()

    # Graficar una variable de los segmentos respecto a otra variable
    plt.figure(figsize=(10, 6))
    for i, segmento in enumerate(segmentos_df):
        # if i != 5 and i != 6 and i != 7 and i != 13 and i != 15 and i != 20 and i != 39 and i != 41:
        plt.plot(segmento[eje_x2], segmento[eje_y2], label=f"Segmento {i+1}")
    plt.xlabel('V (V)')
    plt.ylabel('I (A)')
    plt.title('Gráficas de '+eje_y2+' vs. '+eje_x2+', Archivo: '+str(num_archivo))
    if show_legend==True:
        plt.legend()
    plt.grid(True)
    plt.show() 

    # Graficar una variable de los segmentos respecto a otra variable
    plt.figure(figsize=(10, 6))
    #for i, segmento in enumerate(segmentos_df):
    plt.plot(segmentos_df[10][eje_x2], segmentos_df[10][eje_y2], label=f"Segmento {i+1}")
    plt.xlabel('V (V)')
    plt.ylabel('I (A)')
    plt.title('Gráfica aislada de '+eje_y2+' vs. '+eje_x2+', Archivo: '+str(num_archivo))
    if show_legend==True:
        plt.legend()
    plt.grid(True)
    plt.show() 
def Tiempos(segmentos_df):
    lista_t = []  # Lista para almacenar el primer tiempo de cada barrido

    # Itera sobre cada DataFrame en la lista
    for df in segmentos_df:
        t = df['Tiempo'].iloc[0]  # Extrae el primer valor de la columna 'Tiempo'
        lista_t.append(t)

    return lista_t

def Promediar_Archivos(archivos):
    # Diccionario para almacenar las listas de promedios
    promedios = {}

    # Iterar a través de los archivos
    for archivo in archivos:
        with open(archivo, 'r') as file:
            reader = csv.reader(file)
            data = list(reader)
            
            # Iterar a través de las filas restantes
            for fila in data[1:]:
                etiqueta = fila[0]  # Obtener la etiqueta de la fila
                valores = [float(x) for x in fila[1:]]  # Convertir los valores a flotantes

                # Si la etiqueta aún no está en el diccionario, crear una lista vacía
                if etiqueta not in promedios:
                    promedios[etiqueta] = []

                # Agregar los valores a la lista correspondiente
                promedios[etiqueta].append(valores)

    # Calcular los promedios para cada etiqueta
    for etiqueta, valores in promedios.items():
        promedios[etiqueta] = np.mean(valores, axis=0)

    return promedios

def Graf_Promedios(promedios, etiquetas):
    # Graficar los promedios por separado para cada etiqueta
    for etiqueta in etiquetas:
        plt.figure()  # Crear una nueva figura
        plt.scatter(promedios['lista_t'], promedios[etiqueta])
        plt.xlabel('Tiempo')
        plt.ylabel('Promedio')
        plt.title(etiqueta)
        plt.grid(True)
        plt.show()

def Graf_Iis_Vf(segmentos_df, lista_Iis, lista_Vf):
    vf_list = []  # Lista para almacenar los valores de vf
    t_list = []   # Lista para almacenar los valores de t

    # Crear el primer gráfico para Iis vs. tiempo
    plt.figure(figsize=(10, 6))
    for i, segmento in enumerate(segmentos_df):
        t = segmento['Tiempo'].iloc[-1]
        vf = lista_Vf[i]
        vf_list.append(vf)  # Agregar el valor de vf a la lista
        t_list.append(t)    # Agregar el valor de t a la lista
        plt.scatter(t, lista_Iis[i], label=f"Segmento {i+1}", marker='o')

    plt.xlabel('Tiempo [s]')
    plt.ylabel('Iis [A]')
    plt.title('Gráfica de Iis vs. Tiempo')
    plt.legend()
    plt.grid(True)
    plt.show()
    
    # Crear el segundo gráfico para Vf vs. tiempo
    plt.figure(figsize=(10, 6))
    plt.scatter(t_list, vf_list, label="Valores de vf", marker='x')
    
    plt.xlabel('Tiempo [s]')
    plt.ylabel('Vf [V]')
    plt.title('Gráfica de Vf vs. Tiempo')
    plt.legend()
    plt.grid(True)
    plt.show()

    # Retornar las listas de vf y t
    return vf_list, t_list

def Promedios_Temporales_Entre_Archivos(archivos_csv, n_parametro):

    # Inicializa una lista para almacenar los promedios
    promedios_en_t = []
    # Crea una lista en la que guardará las listas del parámetro de todos los archivos
    lista_listas_parametro = []
    # Crea la lista donde almacenará el tiempo, que para todos los archivos es la misma
    lista_tiempo = []
    
    for archivo_csv in archivos_csv:
        # Lee el archivo CSV en un DataFrame, ignora el header
        data = pd.read_csv(archivo_csv,header=None,skiprows=1)

        # Extrae la fila del parámetro y elimina el primer elemento ('lista_parametro')
        lista_parametro = data.iloc[n_parametro].tolist()
        lista_parametro.pop(0)

        # Agrega esa lista a la lista de listas del parámetro
        lista_listas_parametro.append(lista_parametro)

        # Extrae la lista de tiempos
        lista_tiempo = data.iloc[0].tolist()
        # Elimina el primer elemento ('lista_t')
        lista_tiempo.pop(0)

    # Usa zip para agrupar elementos en la misma posición y calcula el promedio
    for elementos in zip(*lista_listas_parametro):
        promedio = sum(elementos) / len(elementos)
        promedios_en_t.append(promedio)

    return lista_tiempo, promedios_en_t

def Graf_Promedios_Temporales_Entre_Archivos(archivos_csv):
    # Se "importan" las etiquetas de los parámetros
    etiquetas = ["Promedio de t","Promedio de vf","Promedio de vp_sd","Promedio de vp_rectas","Promedio de vp_teorico","Promedio de Iis","Promedio de Ies","Promedio de TeV","Promedio de ne",]

    # Para todos los parámetros
    for n_parametro in range(1,9):
        lista_tiempo, promedios_en_t = Promedios_Temporales_Entre_Archivos(archivos_csv, n_parametro)

        # Crea la gráfica
        plt.figure(figsize=(8, 6))
        plt.plot(lista_tiempo, promedios_en_t, marker='o', linestyle='-', color='b', label='Promedios del parámetro de los archivos')

        plt.xlabel('Tiempo')
        plt.ylabel(etiquetas[n_parametro])
        plt.title('Promedios en función del Tiempo')

        plt.legend()

        plt.grid(True)
        plt.show()

def Promedios_Espaciales_Entre_Archivos(lista_archivos_total,n_parametro,barrido):

    lista_promedios_posicion = []

    # Para cada "set" de archivos (una posición)
    for archivos_csv in lista_archivos_total:

        # Obtiene los promedios en el tiempo
        lista_tiempo, promedios_en_t = Promedios_Temporales_Entre_Archivos(archivos_csv,n_parametro)

        # Extrae el promedio del barrido indicado y el tiempo correspondiente
        promedio_posicion = promedios_en_t[barrido]
        lista_promedios_posicion.append(promedio_posicion)
        tiempo = lista_tiempo[barrido]
    
    return tiempo, lista_promedios_posicion

def Graf_Promedios_Espaciales_Entre_Archivos(archivos_csv,lista_archivos_total,lista_posiciones,barrido):
    # Se "importan" las etiquetas de los parámetros
    etiquetas = ["Promedio de t","Promedio de vf","Promedio de vp_sd","Promedio de vp_rectas","Promedio de vp_teorico","Promedio de Iis","Promedio de Ies","Promedio de TeV","Promedio de ne",]

    # Para todos los parámetros
    for n_parametro in range(1,9):
        tiempo, lista_promedios_posicion = Promedios_Espaciales_Entre_Archivos(lista_archivos_total,n_parametro,barrido)

        # Crea la gráfica
        plt.figure(figsize=(8, 6))
        plt.plot(lista_posiciones, lista_promedios_posicion, marker='o', linestyle='-', color='b', label='Promedios del parámetro en tiempo {0}'.format(tiempo))

        plt.xlabel('Posición')
        plt.ylabel(etiquetas[n_parametro])
        plt.title('Promedios en función de la posición')

        plt.legend()

        plt.grid(True)
        plt.show()